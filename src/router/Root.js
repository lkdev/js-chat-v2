import React, {Component, Fragment} from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Login from '../components/Login';
import HomePage from '../components/HomePage';
import ChatRoom from '../components/ChatRoom';
import ConnectedUsers from '../components/ConnectedUsers';
import LogOut from '../components/LogOut';

export default function Root(props) { //Not using commented in  App.js
    return(
            <Fragment>
                <Route exact path="/home" component={HomePage} />
                {/* <Route exact path="/login" component={Login} /> */}
                <Route exact path='/chats' component={ChatRoom}/>
                <Route exact path='/groups' component={ChatRoom}/>
                <Route exact path='/settings' component={ChatRoom}/>
                <Route exact path='/about--us' component={ChatRoom}/>
                <Route exact path='/users-online' component={ConnectedUsers}/>
                <Route exact path='/logOut' component={LogOut}/>
            </Fragment>
    )
}

