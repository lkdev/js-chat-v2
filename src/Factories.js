const uuidv4= require('uuid/v4');

/** create User */

const createUser=({name= ""} = {})=> (
    {
        id: uuidv4(),
        name
    }
);

/** create Message */

const createMessage=({messages= '', sender= ''}= {})=> (
    {
        id: uuidv4(),
        time: getTime(new Date(Date.now())),
        messages,
        sender
    }
);

/** createChat */

const createChat= ({messages= [], name= 'Community', users= []} = {})=> (
    {
        id: uuidv4(),
        name,
        messages,
        users,
        typingUsers: []


    }
);

/**
 * 
 * @param date {Date}
 * @return a string represented in 24hr time, ie. '1130', '19.20' etc
 * 
 */

 const getTime= (date)=> {
     return `${date.getHours()}:${("0"+date.getMinutes()).slice(-2)}`;
 }


 module.exports= {
     createMessage,
     createChat,
     createUser

 }
