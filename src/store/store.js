/*
 * src/store.js
 * No initialState
*/

import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/rootReducer';
const composeEnhancers = process.env.NODE_ENV == 'development' ?  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;;

export default function configureStore(intialState={}){
    console.log('env',process.env.NODE_ENV);
    return createStore(
        rootReducer,
        intialState, //Not neccessary to define every time
        composeEnhancers(    
            applyMiddleware(thunk)
        )
    );
}
