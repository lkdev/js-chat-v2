import io from 'socket.io-client';
import { USER_CONNECTED, USER_DISCONNECTED, MESSAGE_SEND, LOGOUT } from '../Events';

const socketUrl= 'http://192.168.1.13:3231';
const socket= io(socketUrl);

/**Create Initial Socket Connection with Server */

export const initSocket = (callBack) => {
        socket.on('connect', ()=> {
            callBack && callBack(socket);
        });
}

export const connectUser = (user)=> {
    socket.emit(USER_CONNECTED, user); 
}

export const logOut = () => {
    socket.emit(LOGOUT);
}

export const sendIndividualMessage= (messageObj) => {
    socket.emit(MESSAGE_SEND, messageObj);
}



