import { combineReducers } from 'redux';
import { userData, connectedUsersData } from './userReducer';
export default combineReducers({
 userData,
 connectedUsersData
});