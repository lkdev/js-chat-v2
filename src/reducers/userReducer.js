import { SET_USER_DATA, SET_NEW_CONNECTION, REMOVE_USER } from '../actionTypes';


export const userData = (state = {}, action) => {
    switch (action.type) {
     case SET_USER_DATA :
      return Object.assign({}, action.payload )
     default:
      return state
    }
}

export const connectedUsersData =(state={}, action)=> {
    switch (action.type) {
        case SET_NEW_CONNECTION :
            return Object.assign({}, action.payload );
        case REMOVE_USER: 
            let currentConnectedUsers = JSON.parse(JSON.stringify(state));
            delete currentConnectedUsers[action.payload.name];
            return currentConnectedUsers;
        default:
            return state
       }
}
