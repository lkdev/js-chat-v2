import { SEND_MESSAGE } from '../actionTypes';

export const privateChats = (state={},action) => {
    switch(action.type){
        case SEND_MESSAGE: 
            return Object.assign({}, action.payload);
        default: 
            return state;
    }


}