const io= require('./index.js').io;

const { VERIFY_USER, USER_CONNECTED, USER_DISCONNECTED, MESSAGE_SEND, LOGOUT, NEW_USER_ADDED }= require("../Events");
const { createUser, createMessage, createChat }= require('../Factories');

let connectedUsers= {}; //Object used to keep all the connected users info

module.exports= function(socket) {
    console.log("Socket ID"+socket.id);

    //Verify Username
    socket.on(VERIFY_USER, (nickname, callBack)=> {
        if(isUser(connectedUsers, nickname)) //User already created
            callBack({isUser: true, user: null });
        else //Creating a new user
            callBack({isUser: false, user: createUser({name: nickname})}); /**createUser method will create a user user object {name: '', id: 'sdf1212-wre-13ssa'} format*/
    })

    //User Connects with username

    socket.on(USER_CONNECTED, (user)=> {
        connectedUsers= addUser(connectedUsers,user);

        socket.user = user; /**Used to find a specific user, while user trying to diconnect from socket */
        console.log("connectedUsers", connectedUsers);
        io.emit(NEW_USER_ADDED, connectedUsers); //For emitting an event to all the connected Sockets
    });

    //User disconnects
    socket.on('disconnect', ()=> {
        console.log("user disconnected", socket.user);
        connectedUsers= removeUser(connectedUsers,socket.user);
        io.emit(USER_DISCONNECTED, socket.user || null);
    })

    //User Logouts

    //User Send New Messages
    socket.on(MESSAGE_SEND, (messageData)=> {
        console.log("messgae send",messageData);
    })


}

function addUser(userList, user){
    let newList = Object.assign({}, userList);
    newList[user.name] = user;
    return newList;
}

function removeUser(userList, username){
    let newList = Object.assign({}, userList)
    delete newList[username]
    return newList
  }

/**
 * Check if the user is in list passed in
 * @param {Object} userList 
 * @param {String} username 
 * @return {Object} userList with key value pairs of users
 */

function isUser(userList, username) {
    return username in userList
}