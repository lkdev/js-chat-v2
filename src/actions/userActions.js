import { SET_USER_DATA, SET_NEW_CONNECTION, REMOVE_USER } from '../actionTypes';

export const setUserData= (userData)=> dispatch=> {
    dispatch({
        type: SET_USER_DATA,
        payload: userData
    })
}

export const setNewUserConnection =(connectedUsersInfo= {})=> dispatch=> {
    dispatch({
        type: SET_NEW_CONNECTION,
        payload: connectedUsersInfo
    })
}

export const removeUserFromConncetedUsers = (userInfo= {}) => dispatch=> {
    dispatch(
        {
            type: REMOVE_USER,
            payload: userInfo
        }
    ) 
}

