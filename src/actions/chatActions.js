import { SEND_MESSAGE } from '../actionTypes';


export const sendMessage= (messageObj)=> dispatch=> {
    dispatch({
        type: SEND_MESSAGE,
        payload: messageObj
    })
}