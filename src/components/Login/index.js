import React, { Component } from "react";
// import { Button, FormGroup, FormControl } from "react-bootstrap";
import './index.css';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: ""
    };
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
      console.log("handlechange",event.target.type);
    this.setState({
      [event.target.type]: event.target.value
    });
  }

  handleSubmit = event => {
    event.preventDefault();
    console.log("handle submit");
  }

  render() {
    return (
      <div className="login-wrapper">
        <form onSubmit={this.handleSubmit}>
            <div className="box">
                <h1>Dashboard</h1>

                <input type="email" 
                name="email" 
                placeholder="Email"
                value={this.state.email}
                onChange={this.handleChange}
                // onFocus="field_focus(this, 'email');" 
                // onblur="field_blur(this, 'email');" 
                className="email" />
            
                <input type="password" 
                    name="password" 
                    placeholder="Password"
                    value={this.state.password}
                    onChange={this.handleChange}
                    // onFocus="field_focus(this, 'email');" 
                    // onblur="field_blur(this, 'email');" 
                    className="email" 
                    />
            
                {/* <a href="#"><div className="btn">Sign In</div></a>  */}
                <button
                    className="btn"
                    disabled={!this.validateForm()}
                    type="submit"
                >Sign In</button>

                <a href="#"><div id="btn2">Sign Up</div></a>
            
            </div> 
        
        </form>
      </div>
    );
  }
}
