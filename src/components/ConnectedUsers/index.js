import React, {Component} from 'react';
import './index.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const ConnectedUsers = props => {
    const { connectedUsersData } = props;
    return(
        <div className="connected-users-wrapper">
        <h3>{'Connected Users'}</h3>
        <div className="conneted-users-block">
            {Object.keys(connectedUsersData).length > 0 && 
                Object.keys(connectedUsersData).map((userName, i)=> 
                    <div><span style={{marginRight: '10px'}}>{i+1}</span>{connectedUsersData[userName].name}</div>
                )
            }
            
        </div>
        </div>
    )
}

const mapStateToProps=(state)=> {
    return {
        userData: state.userData,
        connectedUsersData: state.connectedUsersData || {}
    }
}

const mapDispatchToProps= (dispatch)=> {
    //return bindActionCreators({setUserData, setNewUserConnection},dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ConnectedUsers);