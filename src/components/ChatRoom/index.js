import React, {Component, Fragment} from 'react';
import './index.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import usermale from '../../images/usermale.png';
import userfemale from '../../images/userfemale.png';
import { sendIndividualMessage } from '../../clientSocketManager/SocketActions';

class ChatRoom extends Component {
    constructor(props) {
        super(props);
        this.state= {
            newMessage: '',
            userName: 'jithin', //Loggedin username 'me',
            userId: '',
            chatData: [
                {
                    userId: '',
                    name: 'jithin',
                    message: "Hello. How are you today?",
                    time: "11.00"
                },
                {
                    userId: '',
                    name: '',
                    message: "Hey! I'm fine. Thanks for asking?",
                    time: "11.02"
                },
                {
                    userId: '',
                    name: 'jithin',
                    message: "Sweet! So, what do you wanna do today??",
                    time: "11.03"
                },
                {
                    userId: '',
                    name: '',
                    message: "Nah, I dunno. Play soccer.. or learn more coding perhaps?",
                    time: "11.05"
                },

            ]
        };
        this.userInfo= {}
    }

    componentDidMount() {
        console.log("compdidmount", this.props);
        this.userInfo= this.props.userData;
    }

    handleSendMessage = () => {
        const { newMessage }= this.state;
        const getTime= (date= new Date(Date.now()))=> {
            return `${date.getHours()}:${("0"+date.getMinutes()).slice(-2)}`;
        }
        sendIndividualMessage({message: newMessage, time: getTime, userInfo: this.userInfo});

        //this.props.sendNewMessage({newMessage, getTime});
    }

    handleUpdateMessage = (e)=> {
        this.setState({newMessage :e.target.value});
    }

    renderChatData=()=> {
        const { chatData, userName }= this.state;
        return(
            <Fragment>
                {chatData.map((eachChatItem, index)=> 
                    <div key={index} className={`container ${eachChatItem.name == userName ? '' : 'darker'}`}>
                        <img src={eachChatItem.name == userName ? usermale : userfemale} alt="Avatar" className={eachChatItem.name == userName ? "right" : ''} style={{width:"100%"}}/>
                        <p className="chat-text">{eachChatItem.message}</p>
                        <span className={eachChatItem.name == userName ? "time-right" : "time-left"}>{eachChatItem.time}</span>
                    </div>
                    ) 
                }
            </Fragment>
        )
    }

    render() {
        const { newMessage, chatData }= this.state;
        return(
            <div className="individual-chat-wrapper">
                <div>
                <h2>Chat Messages</h2>
                {this.renderChatData()}
                </div>
                <div className="send-new-message-block">
                    <input type="text" value={newMessage}  className="new-message" onChange={this.handleUpdateMessage} />
                    <span className="send-button" onClick={this.handleSendMessage}>{"Send"}</span>
                </div>
            </div>
        )
    }
}

const mapStateToProps=(state)=> {
    return {
        userData: state.userData || {}
    }
}

export default connect(mapStateToProps, null)(ChatRoom);