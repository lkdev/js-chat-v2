import React, {Component, Fragment} from 'react';
import './index.scss';

class LogOut extends Component {
    constructor(props){
        super(props);
    }

    handleLogout = () => {
        console.log('handleLogout');
    }

    render() {
        return(
            <div className='logout-wrapper' style={{marginLeft: '130px'}}>
                <div onClick={this.handleLogout}>{'Logout'}</div>
            </div>
        )
    }
}

export default LogOut;