import React, {Component} from 'react';
import './index.css';
import { BrowserRouter as Router, Route } from "react-router-dom";

import Navbar from '../../common/Navbar';
import Root from '../../router/Root';
import Sidebar from '../../common/SideBar';

class HomeRoute extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Sidebar/>
                    {/* <Navbar/> */}
                    <Root/>
                </div>
            </Router>
            
        )
    }
}

export default HomeRoute;