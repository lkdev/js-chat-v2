import React, {Component} from 'react';
import { VERIFY_USER } from '../Events';

export default class LoginForm extends Component {
    constructor(props){
        super(props);
        this.state= {
            nickName: '',
            error: "",
        };
    }

    setUser= ({isUser, user}) => {
        console.log("setUser", user, isUser);
        if(isUser) {
            this.setError('Sorry!, User name already Taken.');
        } else{
            this.props.setUser(user);
            this.setError("")
        }
    }

    setError= (error)=> {
        this.setState({error});
    }

    handleSubmit=(e)=> {
        e.preventDefault();

        const { socket } = this.props;
        const { nickName } = this.state;
        socket.emit(VERIFY_USER, nickName, this.setUser);
    }

    handleChange = (e)=> {
        this.setState({nickName: e.target.value});
    }

    render() {
        const { nickName, error }= this.state;
        return(
            <div className="login">
                <form onSubmit={this.handleSubmit} className='login-form'>
                <label htmlFor='nickname'>
                    <h2>Got a nickname ?</h2>
                </label>
                <input
                    ref={(input)=> {this.textInput= input}}
                    type='text'
                    id='nickname'
                    value={nickName}
                    onChange={this.handleChange}
                    placeholder={'Nickname'}
                />
                <div className="erorr">{error ? error : null}</div>
                </form>
            </div>
        )
    }
}