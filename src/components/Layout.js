import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setUserData, setNewUserConnection, removeUserFromConncetedUsers } from '../actions/userActions';


//import io from 'socket.io-client';
import { NEW_USER_ADDED, USER_DISCONNECTED } from '../Events';
import LoginForm from './LoginForm';
//import ChatContainer from './chats/ChatContainer';
//import Sidebar from '../common/SideBar';
import HomeRoute from './HomeRoute';
import { initSocket, connectUser, logOut } from '../clientSocketManager/SocketActions';

//const socketUrl= 'http://192.168.43.81:3231';

class Layout extends Component {
    constructor(props) {
        super(props);
        this.state= {
            socket: null,
            user: null
        }
    }

    componentWillMount() {
        console.log("componentWillMount");
        initSocket((socket)=> {
            console.log("connected");
            this.setState({socket});
            socket.on(NEW_USER_ADDED,(userData)=> { //Event will trigger for all the sockets while adding a new user to the room
                console.log("new userData",userData);
                this.props.setNewUserConnection(userData);
            });
            socket.on(USER_DISCONNECTED, (userData)=> {
                console.log("fetch disconncet in client",userData);
                this.props.removeUserFromConncetedUsers(userData);
            })
        });
        
        //this.initSocket();
    }

    componentDidMount() {
        
    }

    componentWillReceiveProps(nextProps, nextState) {
        if(nextProps.userData !== this.state.user) {
            this.setState({user: nextProps.userData});
        }
    }

    //initSocket = ()=> {
        // const socket= io(socketUrl);
        // socket.on('connect', ()=> {
        //     console.log("connected");
        // });
        
    //}

    setUser=(user)=> {
        //const { socket }= this.state;
        console.log("setUser layout", user);
        //socket.emit(USER_CONNECTED, user);
        connectUser(user);
        this.props.setUserData(user);
        //this.setState({ user });
    }

    logOut= ()=> {
        const { socket }=this.state;
        //socket.emit(LOGOUT);
        logOut();
        this.setState({user: null});

    }

    render() {
        //const { title }= this.props;
        const { socket, user } =this.state;
        return (
            <div className="container">
            {user ? 
            <HomeRoute/>
            // <Sidebar/>
             // <ChatContainer socket={socket} user={user} logout={this.logOut}/>
            : <LoginForm 
                socket={socket} 
                setUser={this.setUser}
            />
            }

            </div>
        )
    }
}

const mapStateToProps=(state)=> {
    return {
        userData: state.userData
    }
}

const mapDispatchToProps= (dispatch)=> {
    return bindActionCreators({
        setUserData, 
        setNewUserConnection,
        removeUserFromConncetedUsers
    },dispatch);
}



export default connect(mapStateToProps, mapDispatchToProps)(Layout);