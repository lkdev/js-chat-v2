import React, {Component} from 'react';
import './index.css';
import { Link, NavLink  } from 'react-router-dom';
import ChatRoom from '../../components/ChatRoom';

class Sidebar extends Component {
    render() {
        return(
            <div className="sidebar-wrapper">
                <div className="sidebar">
                    {/* location = { pathname: '/react' } */}
                    <NavLink to="/home" activeClassName="fa fa-fw fa-home">
                        Home
                    </NavLink>
                    {/* // <a href='/react' className='hurray'>Home</a> */}
                    <NavLink to='/chats' activeClassName="fa fa-fw fa-user">
                        Chats
                    </NavLink>
                    <NavLink to="/users-online" activeClassName="fa fa-fw fa-users">
                        OnLine Users
                    </NavLink>
                    <NavLink to="/groups" activeClassName="fa fa-fw fa-users">
                        Groups
                    </NavLink>
                    <NavLink to="/settings" activeClassName="fa fa-fw fa-wrench">
                        Settings
                    </NavLink>
                    <NavLink to="/about-us" activeClassName="fa fa-fw fa-envelope"> 
                        About Us
                    </NavLink>
                    <NavLink to="/logOut" activeClassName="fa fa-fw fa-wrench"> 
                        LogOut
                    </NavLink>
                    {/* <a href="#home"><i className="fa fa-fw fa-home"></i> Home</a>
                    <a href="#clients"><i className="fa fa-fw fa-user"></i> Chats</a>
                    <a href="#clients"><i className="fa fa-fw fa-users"></i> Groups</a>
                    <a href="#services"><i className="fa fa-fw fa-wrench"></i> Settings</a>
                    <a href="#contact"><i className="fa fa-fw fa-envelope"></i> About Us</a> */}
                    </div>

                    {/* <div className="main">
                    <h2>Sidebar with Icons</h2>
                    <p>This side navigation is of full height (100%) and always shown.</p>
                    </div> */}
            </div>
        )
    }
}

export default Sidebar;