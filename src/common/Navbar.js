import React from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css'; /**Need to change this structure */

export default function Navbar() {
    return(
        // <nav>
        //     <ul>
        //         <li>
        //         <Link to="/">Home</Link>
        //         </li>
        //         <li>
        //         <Link to="/login">Login</Link>
        //         </li>
        //         <li>
        //         <Link to="/users/">Users</Link>
        //         </li>
        //     </ul>
        // </nav>

        <nav className="navbar">
            <span className="navbar-toggle" id="js-navbar-toggle">
                    <i className="fas fa-bars"></i>
                </span>
            <a href="#" className="logo">JS CHAT</a>
            <ul className="main-nav" id="js-menu">
            <li>
                <a href="/" className="nav-links">Home</a>
            </li>
            <li>
                <a href="/login" className="nav-links">Login</a>
            </li>
            <li>
                <a href="/chatroom" className="nav-links">Chat Room</a>
            </li>
            <li>
                <a href="#" className="nav-links">Contact Us</a>
            </li>
            <li>
                <a href="#" className="nav-links">Blog</a>
            </li>
            </ul>
        </nav>
    )
}